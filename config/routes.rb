Rails.application.routes.draw do

  #errrs routes
  match "/404", :to => "errors#error_404", :via => :all
  match "/500", :to => "errors#error_500", :via => :all
  match "/422", :to => "errors#error_422", :via => :all

  #routes for Devise
  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks'}

  authenticated :user do
      #routes for root AUTHENTICATED
      get '/', :to => 'home#index', as:'home'
  end


  #routes for root NOT AUTHENTICATED
  root 'home#splash'

  #routes that require authentication
  authenticated do

    #routes for resources
    resources :matches #, except: [:index]
    resources :free_matches #, except: [:index]
    resources :teams #, except: [:index]
    resources :users, only: [:show]
    resources :notifications do
      collection do
        post :mark_as_read
      end
    end


    # routes for my autocomplete via Ajax
    get '/autocomplete/:q', :to => 'searches#autocomplete_users',as:'autocomplete_users'

    # routes for the searches
    get '/search', :to =>'searches#search',as: 'search'

    #routes for model User
    match 'users/:id' => 'users#show', via: :get
    match 'users/:id/settings' => 'users#settings', via: :get, as: 'user_settings'
    get 'users/:id' => 'users#show'
    get 'users/:id/set_soccer_role', :to =>'users#set_soccer_role', as: 'set_soccer_role'
    post 'users/:id/set_soccer_role', :to =>'users#update_soccer_role', as: 'update_soccer_role'
    get 'users/:id/matches_info', :to =>'users#matches_info', as: 'matches_info'

    #routes for model Team
    get 'teams/:id/edit_members_roles', :to => 'teams#edit_members_roles', as: 'edit_members_roles'
    post 'teams/:id/update_members_roles', :to => 'teams#update_members_roles', as: 'update_members_roles'
    get 'teams/:id/select_new_players', :to => 'teams#select_new_players', as: 'select_new_players'
    post 'teams/:id/invite', :to => 'teams#invite', as: 'invite'
    post 'teams/:id/add_player', :to => 'teams#add_player', as: 'add_team_player'

    #routes for model Match
    get '/new_match_set_type', :to => 'matches#new_set_type', as:'new_match_set_type'
    post 'matches/:id/set_match_scores', :to =>'matches#set_match_scores', as: 'set_match_scores'
    post 'matches/:id/accept_team_request', :to => 'matches#accept_request', as: 'accept_match_request'
    get 'matches/:id/set_score', :to => 'matches#set_score', as: 'set_match_score'
    post 'matches/:id/update_score', :to => 'matches#update_score', as: 'update_match_score'

    #routes for model FreeMatch
    post 'free_match/:id/add_player', :to => 'free_matches#add_player', as: 'add_free_match_player'
    post 'free_matches/:id/request', :to => 'free_matches#request_free_match', as: 'request_free_match'
    post 'free_matches/:id/accept_request/:uid', :to => 'free_matches#accept_request', as: 'accept_free_match_request'

    #routes to Follow/Unfollow via Ajax
    get '/follow/:following_id/:follower_id', :to => 'users#follow', as: 'follow'
    get '/unfollow/:following_id/:follower_id', :to => 'users#unfollow', as: 'unfollow'

  end
end
