FactoryBot.define do
  factory :random_team, class: Team do
    name Faker::Football.team
    calcetto [true, false].sample
    calciotto [true, false].sample
  end
end
