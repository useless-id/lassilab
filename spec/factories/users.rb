FactoryBot.define do

  factory :user do
    name "Stefano"
    email "stefanoelatini@hotmail.it"
    password "111111"
  end

  factory :random_user, class: User do
    name Faker::Name.name
    email Faker::Internet.email
    password "222222"
    password_confirmation "222222"
  end
end
