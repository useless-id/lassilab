require 'rails_helper'

feature "signing up and in" do
  let(:user) {build(:user)}

  def fill
    p user.name
    fill_in "user[name]", with: user.name
    p user.email
    fill_in "user[email]", with: user.email
    p user.password
    fill_in "user[password]", with: user.password
    fill_in "user[password_confirmation]", with: user.password
  end

  scenario "clicking the login button and the signup" do
    Capybara.current_session.driver.header('Accept-Language','it')
    visit new_user_registration_path
    expect(page).to have_content("Sign up")
    fill
    click_button "Sign up"
    expect(page).to have_content("Logout")
  end

end
