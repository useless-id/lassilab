class CreateActivities < ActiveRecord::Migration[5.1]
  def change
    create_table :activities do |t|
      t.integer :actor_id
      t.string :action
      t.integer :object_id
      t.string :object_type
      t.timestamps
    end
  end
end
