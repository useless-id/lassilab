class AddNameToFreeMatch < ActiveRecord::Migration[5.1]
  def change
    add_column :free_matches, :name, :string
  end
end
