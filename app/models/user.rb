class User < ApplicationRecord
  include Friendlyable

  mount_uploader :avatar, AvatarUploader

  after_create :assign_default_role
  after_save :check_avatar
  rolify
  has_and_belongs_to_many :free_matches
  has_and_belongs_to_many :teams
  has_many :notifications, foreign_key: :recipient_id

  has_many :follower_relationships, foreign_key: :following_id, class_name: 'Follow'
  has_many :followers, through: :follower_relationships, source: :follower

  has_many :following_relationships, foreign_key: :follower_id, class_name: 'Follow'
  has_many :following, through: :following_relationships, source: :following



  validates_presence_of :name
  validates_presence_of :email, uniqueness:true

  def assign_default_role
    add_role(:role)
  end

  def get_soccer_role
    if self.soccer_role == 1
      return "portiere"
    elsif self.soccer_role == 2
      return "difensore"
    elsif self.soccer_role == 3
      return "ala"
    elsif self.soccer_role == 4
      return "pivot"
    elsif soccer_role == 0
      return "universale"
    else
      return "nessun ruolo"
    end
  end

  def check_avatar
    if self.avatar.present? == false
      identicon_url = "http://www.gravatar.com/avatar/"+self.hash.to_s.tr('-', '')+"?s=512&d=identicon"
      p identicon_url
      tempfile = open(identicon_url)
      #self.remove_ = tempfile
      self.remote_avatar_url = identicon_url
      self.save!
    end
  end
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable,
         omniauth_providers: %i[facebook]

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
    user.email = auth.info.email
    user.password = Devise.friendly_token[0,20]
    user.name = auth.info.name   # assuming the user model has a name
    user.avatar = auth.info.image # assuming the user model has an image
    # If you are using confirmable and the provider(s) you use validate emails,
    # uncomment the line below to skip the confirmation emails.
    # user.skip_confirmation!
    end
  end

end
