class Team < ApplicationRecord
  mount_uploader :avatar, AvatarUploader
  resourcify
  rolify :role_cname => 'TeamRole'
  has_and_belongs_to_many :users
  has_and_belongs_to_many :matches
  has_many :team_notifications, foreign_key: :recipient_id

  validates :name, uniqueness:{message: "già scelto"}
  validates :name, presence:{message: "mancante"}
  #after_create :create_identicon


end
