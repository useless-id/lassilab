class AutocompleteUsers
  constructor: ->
    $('.input-field.autocomplete-field').append('
    <ul id="autocomplete-options-e3684425-f343-79f9-ffa7-b159158bc340"
    class="collection autocomplete-content-custom" >
    ')

    $("ul.autocomplete-field").hide()
    $("input.autocompletex").bind 'keyup keydown', (e) ->
      $("ul.autocomplete-content-custom").html('')
      $.ajax(
        url: "/autocomplete/#{$("input.autocompletex").val()}.json"
        dataType: "JSON"
        method: "GET"
        success:
          (data, textStatus, jqXHR) ->
            for i in data.res
              if data.res.length > 0
                $("ul.autocomplete-content-custom").css("display", "block");
                #@autocompletes.append('<li><span><span class="highlight">A</span>pple</span></li></ul>')
                $("ul.autocomplete-content-custom").append('
                <li data-id="'+i.id+'" class="collection-item autocomplete-result avatar">
                <img src="'+i.avatar.url+'" class="circle">
                <span class="title username">'+i.name+'</span>
                <p>RUOLO<br>
                </p>
                </li>
                ')
              else
                $("ul.autocomplete-content-custom").html('')
                $("ul.autocomplete-content-custom").css("display", "none");
            $('li.autocomplete-result').on 'click', (e) ->
              $("input.autocompletex").val('')
              name = $(this).find('span.username').html()
              avatar = $(this).find('img.circle').attr('src')
              id = $(this).data('id')
              $("ul.autocomplete-content-custom").html('')
              $('.chips-container').append('
              <div class="chip" tabindex="0">
                <img src="'+avatar+'" alt="Contact Person">
                '+name+'
                <i class="close material-icons">close</i>
              </div>
              ')
              $('input[type="submit"]').removeClass('disabled')
              $('.chips-container').find('input.input').remove()
              $('.chips-container').removeClass('input-field')
              $('<input>').attr('type','hidden').attr('name','invited_users[]').attr('value',id).appendTo('form')
              $('.chip .close').on 'click', (e) ->
                id_aux = $(this).closest('div.chip').data('id')
                $('input[name="invited_users[]"]').attr('id',id_aux).remove()

                $(this).closest('div.chip').remove()
                numItems = $('.chip').length
                if numItems is 0
                  $('input[type="submit"]').addClass('disabled')

      )


$(document).on "turbolinks:load", ->
  new AutocompleteUsers
