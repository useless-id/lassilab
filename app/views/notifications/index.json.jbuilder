json.array! @notifications do |notification|
  json.actor notification.actor.name
  json.actor_type notification.actor.class.name
  json.actor_id notification.actor.id
  json.action notification.action
  json.notifiable do
    json.type "#{notification.notifiable.class.name}"
  end
  if notification.notifiable.class != NilClass
  json.url "#{polymorphic_url(notification.notifiable)}"
  end
end
