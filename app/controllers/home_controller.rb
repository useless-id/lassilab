class HomeController < ApplicationController


  def splash
      render(:layout => "layouts/splash")
  end

  def index
    if current_user && current_user.sign_in_count == 1 && !current_user.soccer_role.present?
      redirect_to set_soccer_role_path(current_user)
    end
    @users = current_user.following
    @collection = Activity.where(actor_id: current_user.id)
    if @users.empty? == false
      @users.each do |friendz|
        @collection = @collection + Activity.where(actor_id: friendz.id)
      end
    else
      @collection = Activity.none
    end
  end

end
