class UsersController < ApplicationController
  def show
    @user = User.friendly.find(params[:id])
    @teams = User.friendly.find(params[:id]).teams
    @activity = Activity.where(actor: @user)
  end

  def set_soccer_role
    @user = User.friendly.find(params[:id])
  end

  def update_soccer_role
    @user = User.friendly.find(params[:id])
    @user.soccer_role = params[:soccer_role].to_i
    @user.save
    redirect_to user_path(@user), notice: 'Ruolo aggiornato!'
  end

  def follow
    @follower = User.friendly.find(params[:follower_id])
    @following = User.friendly.find(params[:following_id])
    if Follow.where(follower_id: @follower.id, following_id: @following.id).empty? == true
    Follow.create(follower_id: @follower.id, following_id: @following.id)
    render json: {
      "res" => "done"
    }
    end
  end

  def unfollow
    @follower = User.friendly.find(params[:follower_id])
    @following = User.friendly.find(params[:following_id])
    @follow = Follow.where(follower_id: @follower.id, following_id: @following.id).first
    @follow.delete
    render json: {
      "res" => "done"
    }
  end

  def matches_info
    @user = User.friendly.find(params[:id])
    team_ids = @user.teams.pluck(:id)
    @won_matches = Match.where(winner_team_id: team_ids).group_by { |m| m.date.beginning_of_month }
    @lost_matches = Match.where.not(winner_team_id: team_ids).group_by { |m| m.date.beginning_of_month }
    render json: {
      "won" => @won_matches.as_json,
      "lost" => @lost_matches.as_json
    }
    #render json: @user.teams.where
  end
end
