class MatchesController < ApplicationController
  before_action :set_match, only: [:show, :edit, :update, :destroy,:set_match_scores]
  before_action :authenticate_user!

  # GET /matches/1/set_score
  def set_score
    @match = Match.find(params[:id])
    if Match.find(params[:id]).date.past? == false
      redirect_to match_path(Match.find(params[:id])), notice: 'Il match non è ancora finito!'
    end
    @match.teams.each do |team|
      if(team.has_role?(:manager, @match))
        @home_team = team
      else
        @host_team = team
      end
    end
  end

  #GET
          def accept_request
    @match = Match.find(params[:id])
    @home_team = Team.with_role(:manager,@match)
    @host_team = TeamNotification.where(actor: @home_team, action: "invited",notifiable: @match).first.recipient
    @match.teams << @host_team
    @host_team.add_role(:member,@match)
    TeamNotification.where(actor: @home_team, action: "invited",notifiable: @match).first.delete
    redirect_to @match, notice: "La tua squadra ha confermato l'invito"
  end

  #POST
  def set_match_scores
    if Match.find(params[:id]).date.past? == false
      redirect_to match_path(Match.find(params[:id])), notice: 'Il match non è ancora finito!'
    end
    if @match.home_score != nil
      redirect_to match_path(Match.find(params[:id])), notice: 'Hai già settato il punteggio di questa partita'
    end
    @match.home_score = params[:home_score]
    @match.host_score = params[:host_score]
    if(params[:host_score].to_i > params[:home_score].to_i)
      @host_team = Team.with_role(:team,@match)
      @match.winner_team_id = @host_team.first.id
      @match.save
    else
      @home_team = Team.with_role(:manager,@match)
      @match.winner_team_id = @home_team.first.id
      @match.save
    end
    redirect_to @match, notice: 'Punteggio salvato correttamente.'
  end

  # POST
  def update_score
    respond_to do |format|
      if @match.update(match_params)
        format.html { redirect_to @match, notice: 'Match was successfully updated.' }
        format.json { render :show, status: :ok, location: @match }
      else
        format.html { render :edit }
        format.json { render json: @match.errors, status: :unprocessable_entity }
      end
    end
  end
  # GET /matches
  # GET /matches.json
  def index
  #  @matches = Match.all
  redirect_to root_path
  end

  # GET /matches/1
  # GET /matches/1.json
  def show
    if(@match.teams.count >0)
      @match.teams.each do |team|
        team.users.each do |user|
          if(user.id == current_user.id)
            @user_team = Team.find(team.id)
          end
        end
      end
      @match.teams.each do |team|
        if(team.has_role?(:manager, @match))
          @home_team = team
        else
          @host_team = team
        end
      end
    end
    if(@match.teams.count ==1)
      @home_team = Team.with_role(:manager, @match).first
      @host_team = TeamNotification.where(actor: @home_team, action: "invited",notifiable: @match).first.recipient
    end
  end

  # GET /matches/new
  def new
    if current_user.teams.count != 0
      if params[:type].present? == false
        redirect_to new_match_set_type_path
      else
      @type = params[:type]
      @match = Match.new
      end
    else
      redirect_to root_path, notice: 'Non hai ancora una squadra!' 
    end
  end

  # GET /matches/1/edit
  def edit
    if Match.find(params[:id]).date.past? == true && @match.home_score != nil
      redirect_to match_path(Match.find(params[:id])), notice: 'Non puoi modificare ua partita finita'
    end
    if(@match.teams.count >1)
      @match.teams.each do |team|
        team.users.each do |user|
          if(user.id == current_user.id)
            @user_team = Team.find(team.id)
          end
        end
      end
      @match.teams.each do |team|
        if(team.has_role?(:manager, @match))
          @home_team = team
        else
          @host_team = team
        end
      end
    end
    if(@match.teams.count ==1)
      @home_team = Team.with_role(:manager, @match).first
      @host_team = TeamNotification.where(actor: @home_team, action: "invited",notifiable: @match).first.recipient
    end
  end

  def new_set_type
  end

  # POST /matches
  # POST /matches.json
  def create

    @match = Match.new(match_params)

    # Quando creo un primo match senza assegnarlo ad una squadrea in casa e ospite assegno un ruolo temporaneo all'utente che lo crea

    respond_to do |format|
      p params[:match][:date]
      #@date_parsed = DateTime.strptime(params[:match][:date], "%Y-%m-%d %H:%M:S")
      @match.date = DateTime.parse(params[:match][:date])
      if @match.save
        match = @match
        Activity.create(actor: current_user, action: "created",object: match)
        @home_team = Team.where(name: params[:home]).first
        @home_team.add_role(:manager, @match)
        @match.teams << @home_team
        @host_team = Team.where(name: params[:host]).first
        #@host_team.add_role(:team,@match)
        #@match.teams << @host_team
        TeamNotification.create(recipient: @host_team, actor: @home_team, action: "invited",notifiable: @match)
        current_user.add_role(:manager, @match)
        format.html { redirect_to @match, notice: 'Match was successfully created.' }
        format.json { render :show, status: :created, location: @match }
      else
        format.html { render :new }
        format.json { render json: @match.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /matches/1
  # PATCH/PUT /matches/1.json
  def update
    old_home_team = Team.with_role(:manager,@match).first
    new_home_team = Team.where(name: params[:home]).first
    if @match.teams.count == 1
      old_host_team = TeamNotification.where(actor: @home_team, action: "invited",notifiable: @match).first.recipient
      new_host_team = Team.where(name: params[:host]).first
      if old_host_team != new_host_team
        TeamNotification.where(actor: old_home_team, action: "invited",notifiable: @match).delete
        TeamNotification.create(recipient: new_host_team, actor: new_home_team, action: "invited",notifiable: @match)
      end
      if old_home_team != new_home_team
        old_home_team.remove_role(:manager, @match)
        @match.teams.delete_all
        @match.teams << new_home_team
        new_home_team.add_role(:manager,@match)
      end
    elsif @match.teams.count == 2
      new_host_team = Team.where(name: params[:host]).first
      old_host_team = Team.with_role(:team,@match)
      old_home_team.remove_role(:manager,@match)
      old_host_team.remove_role(:manager,@match)
      new_home_team.add_role(:manager,@match)
      @match.teams.delete_all
      @match.teams << new_home_team
      TeamNotification.create(recipient: new_host_team, actor: new_home_team, action: "invited",notifiable: @match)
    end
    @match.date = DateTime.parse(params[:match][:date])
    respond_to do |format|
      if @match.update(match_params)
        @home_team = Team.with_role(:manager,@match)
        @host_team = TeamNotification.where(actor: new_home_team, action: "invited",notifiable: @match).first.recipient
        format.html { redirect_to @match, notice: 'Match was successfully updated.' }
        format.json { render :show, status: :ok, location: @match }
      else
        format.html { render :edit }
        format.json { render json: @match.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /matches/1
  # DELETE /matches/1.json
  def destroy
    TeamNotification.where(notifiable:@match).each do |notification|
      notification.delete
    end
    Activity.where(object:@match).each do |activity|
      activity.delete
    end
    @match.teams.each do |team|
      if team.has_role?(:manager, @match)
        team.remove_role(:manager,@match)
      end
      if team.has_role?(:member, @match)
        team.remove_role(:member,@match)
      end
    end
    @match.delete
    respond_to do |format|
      format.html { redirect_to matches_url, notice: 'Match was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_match
      @match = Match.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def match_params
      params.require(:match).permit(:date, :latitude, :longitude, :host, :home, :host_score, :home_score,:tipologia,:type)
      #params.fetch(:match, {})
    end
end
