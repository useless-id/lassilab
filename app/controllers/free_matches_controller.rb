class FreeMatchesController < ApplicationController
  before_action :set_free_match, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user! #-> routes to the login / signup if not authenticated

  # GET /free_matches
  # GET /free_matches.json
  def index
    #@free_matches = FreeMatch.all
    #@user = current_user
    redirect_to root_path
  end

  # GET /free_matches/1
  # GET /free_matches/1.json
  def show
    @free_match = FreeMatch.find(params[:id])
  end

  # GET /free_matches/new
  def new
    @users_to_invite = User.where('id != ?', current_user.id)
    @free_match = FreeMatch.new
  end

  # GET /free_matches/1/edit
  def edit
    @users_to_invite = User.where('id != ?', current_user.id)
    @free_match = FreeMatch.find(params[:id])
    @free_march_users = @free_match.users
    if !current_user.has_role?(:manager, @free_match)
      return redirect_to("/free_matches")
    end
  end

  # POST /free_matches
  # POST /free_matches.json
  def create
    @free_match = FreeMatch.new(free_match_params)
    @user = current_user
    respond_to do |format|
      if @free_match.save
        free_match = @free_match
        Activity.create(actor: current_user, action: "created",object: free_match)
        current_user.free_matches << @free_match
        current_user.add_role(:manager, @match)
        params[:player_to_add].each do |email|
          user = User.where(email: email).first
          if user.present? && Notification.where(recipient: user, actor: current_user, action: "invited",notifiable: free_match).empty? == true
            free_match = @free_match
            Notification.create(recipient: user, actor: current_user, action: "invited",notifiable: free_match)
          end
        end

        format.html { redirect_to @free_match, notice: 'Free match was successfully created.' }
        format.json { render :show, status: :created, location: @free_match }
      else
        format.html { render :new }
        format.json { render json: @free_match.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /free_matches/1
  # PATCH/PUT /free_matches/1.json
  def update
    if user_signed_in? && current_user.has_role?(:manager, @free_match)
      params[:player_to_add].each do |email|
        user = User.where(email: email).first
        if user.present? && Notification.where(recipient: user, actor: current_user, action: "invited",notifiable: free_match).empty? == true
          free_match = @free_match
          Notification.create(recipient: user, actor: current_user, action: "invited",notifiable: free_match)
        end
      end
    end
    respond_to do |format|
      if @free_match.update(free_match_params)
        format.html { redirect_to @free_match, notice: 'Free match was successfully updated.' }
        format.json { render :show, status: :ok, location: @free_match }
      else
        format.html { render :edit }
        format.json { render json: @free_match.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /free_matches/1
  # DELETE /free_matches/1.json
  def destroy
    Notification.where(notifiable:@free_match).each do |notification|
      notification.delete
    end
    Activity.where(object:@free_match).each do |activity|
      activity.delete
    end
    @free_match.users.each do |user|
      if user.has_role?(:manager, @free_match)
        user.remove_role(:manager,@free_match)
      end
      if user.has_role?(:member, @free_match)
        user.remove_role(:member,@free_match)
      end
    end
    @free_match.delete
    respond_to do |format|
      format.html { redirect_to free_matches_url, notice: 'Free match was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def add_player
    @free_match = FreeMatch.find(params[:id])
    if Notification.where(recipient: current_user, notifiable: @free_match, action:"invited").present?
      @free_match.users << current_user
      current_user.add_role :member, @free_match
      Notification.where(action:"invited",notifiable:@free_match,recipient:current_user).first.delete
      redirect_to free_match_path(@free_match)
    end
  end

  def accept_request
    @free_match = FreeMatch.find(params[:id])
    user = User.friendly.find(params[:uid])
    @free_match.users << user
    user.add_role :member, @free_match
    Notification.where(action:"requested",notifiable:@free_match,actor:user).first.delete
    redirect_to free_match_path(@free_match), notice: 'Invito Accettato'
  end

  def request_free_match
    @free_match = FreeMatch.find(params[:id])
    users = User.with_role(:manager,@free_match)
    users.each do |user|
      if Notification.where(
        recipient: user,
        actor:current_user,
        notifiable: @free_match,
        action:"requested").empty?

        free_match = @free_match
        Notification.create(recipient: user, actor: current_user, action: "requested",notifiable: free_match)
        Activity.create(actor: current_user, action: "requested",object: free_match)
      end
    end
    redirect_to root_path, notice: 'Richiesta spedita.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_free_match
      @free_match = FreeMatch.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def free_match_params
      #params.fetch(:free_match, {})
      params.require(:free_match).permit(:date, :latitude, :longitude, :player_to_add,:uid,:name)
    end
end
